# Site Update Notifier Server#

Implement personalized web-service server that checks specified web-resources for update and notifies owner if resource was updated since last check.

Server is written on Java using Spring Boot framework. MySQL is used as database storage.

### Bulid ###

* Build project with: mvn clean package
* Run server with: java -jar target/siteupdatenotifier-1.0-SNAPSHOT.jar


### Contacts ###

adippel@thumbtack.net