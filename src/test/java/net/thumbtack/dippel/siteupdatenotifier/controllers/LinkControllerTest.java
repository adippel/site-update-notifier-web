package net.thumbtack.dippel.siteupdatenotifier.controllers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import net.thumbtack.dippel.siteupdatenotifier.SiteUpdateNotifierServer;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.DeletePageRequest;
import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserPageDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Page;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;


/**
 * Created by adippel on 09.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SiteUpdateNotifierServer.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class LinkControllerTest {

    private static String token;
    @Autowired
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Autowired
    @Qualifier("primaryUserPageRepository")
    private UserPageDAO userPageRepository;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void setUp(){
        userRepository.deleteAll();
        sessionRepository.deleteAll();
        userPageRepository.deleteAll();

        userRepository.create(new User("username1", "password", "artdippel@mail.ru"));
        final Session session = new Session("username1");
        session.setToken(UUID.randomUUID().toString());
        sessionRepository.create(session);

        token = session.getToken();
        RestAssured.port = serverPort;
    }

    @After
    public void clear(){
        userRepository.deleteAll();
        sessionRepository.deleteAll();
        userPageRepository.deleteAll();
    }

    @Test
    public void testAddPage() throws MalformedURLException {
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 0 userpages", 0, userPages.size());
        RestAssured.given().header("token", token)
                .request().body(page).contentType(ContentType.JSON)
                .when().post("/api/link")
                .then().statusCode(HttpStatus.SC_CREATED);
        userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 1 userpages", 1, userPages.size());
    }

    @Test
    public void testAddPageWithWrongToken() throws MalformedURLException {
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 0 userpages", 0, userPages.size());
        RestAssured.given().header("token", token+"wrong")
                .request().body(page).contentType(ContentType.JSON)
                .when().post("/api/link")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 0 userpages", 0, userPages.size());
    }

    @Test
    public void testAddPageWithWrongURL() throws MalformedURLException {
        final Page page = new Page(new URL("http://9gagqwer.com/"), "fun");
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 0 userpages", 0, userPages.size());
        RestAssured.given().header("token", token)
                .request().body(page).contentType(ContentType.JSON)
                .when().post("/api/link")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 0 userpages", 0, userPages.size());
    }

    @Test
    public void testGetPages() throws MalformedURLException {
        final Page page = new Page(new URL("http://9gag.com/"), "fun");
        final Page page1 = new Page(new URL("http://docs.mongodb.org/ecosystem/use-cases/storing-log-data/"), "doc");
        userPageRepository.create(new UserPage(page, "username1"));
        userPageRepository.create(new UserPage(page1, "username1"));
        RestAssured.given().header("token", token)
                .when().get("/api/link")
                .then().statusCode(HttpStatus.SC_OK)
                .body("pages.type", Matchers.hasItems("fun","doc"));
    }

    @Test
    public void testGetPagesWithNoPagesToWatch() throws MalformedURLException{
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        final Page page1 = new Page(new URL("http://docs.mongodb.org/ecosystem/use-cases/storing-log-data/"), "doc");
        userPageRepository.create(new UserPage(page, "username1"));
        userPageRepository.create(new UserPage(page1, "username1"));
        userRepository.create(new User("username2", "password", "aadippel@mail.ru"));
        final Session session = new Session("username2");
        sessionRepository.create(session);
        RestAssured.given().header("token", sessionRepository.findByUsername("username2").getToken())
                .when().get("/api/link")
                .then().statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void testGetPagesWithWrongToken() throws MalformedURLException{
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        final Page page1 = new Page(new URL("http://docs.mongodb.org/ecosystem/use-cases/storing-log-data/"), "doc");
        userPageRepository.create(new UserPage(page, "username1"));
        userPageRepository.create(new UserPage(page1, "username1"));
        RestAssured.given().header("token", token+"wrong")
                .when().get("/api/link")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testDeletePage() throws MalformedURLException {
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        final Page page1 = new Page(new URL("http://docs.mongodb.org/ecosystem/use-cases/storing-log-data/"), "doc");
        userPageRepository.create(new UserPage(page, "username1"));
        userPageRepository.create(new UserPage(page1, "username1"));
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 2 userpages", 2, userPages.size());
        final DeletePageRequest deletePageRequest = new DeletePageRequest(page.getUrl());
        RestAssured.given()
                .header("token", token)
                .body(deletePageRequest).contentType(ContentType.JSON)
                .when().delete("/api/link")
                .then().statusCode(HttpStatus.SC_OK);
        userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 1 userpage", 1, userPages.size());
    }

    @Test
    public void testDeletePageWithWongToken() throws MalformedURLException {
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        final Page page1 = new Page(new URL("http://docs.mongodb.org/ecosystem/use-cases/storing-log-data/"), "doc");
        userPageRepository.create(new UserPage(page, "username1"));
        userPageRepository.create(new UserPage(page1, "username1"));
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 2 userpages", 2, userPages.size());
        final DeletePageRequest deletePageRequest = new DeletePageRequest(page.getUrl());
        RestAssured.given()
                .header("token", token+"wrong")
                .body(deletePageRequest).contentType(ContentType.JSON)
                .when().delete("/api/link")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 2 userpage", 2, userPages.size());
    }

    @Test
    public void testDeleteOtherUserUrl() throws MalformedURLException {
        final Page page = new Page(new URL("http://yandex.ru/"), "fun");
        final Page page1 = new Page(new URL("http://docs.mongodb.org/ecosystem/use-cases/storing-log-data/"), "doc");
        userPageRepository.create(new UserPage(page, "username1"));
        userPageRepository.create(new UserPage(page1, "username1"));
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 2 userpages", 2, userPages.size());

        final User user = new User("username2", "password", "aadippel@mail.ru");
        userRepository.create(user);
        final Session session = new Session(user.getUsername());
        sessionRepository.create(session);
        final String newtoken = session.getToken();

        final DeletePageRequest deletePageRequest = new DeletePageRequest(page.getUrl());
        RestAssured.given()
                .header("token", newtoken)
                .body(deletePageRequest).contentType(ContentType.JSON)
                .when().delete("/api/link")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        userPages = userPageRepository.getAll();
        Assert.assertEquals("User repository must contain 2 userpages", 2, userPages.size());
    }







}