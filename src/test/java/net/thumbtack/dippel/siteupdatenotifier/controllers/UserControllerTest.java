package net.thumbtack.dippel.siteupdatenotifier.controllers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import net.thumbtack.dippel.siteupdatenotifier.SiteUpdateNotifierServer;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.UserLoginRequest;
import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by adippel on 09.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SiteUpdateNotifierServer.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class UserControllerTest {


    @Autowired
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;


    @Value("${local.server.port}")
    private int serverPort;
    @Before
    public void setUp(){
        userRepository.deleteAll();
        sessionRepository.deleteAll();
        RestAssured.port = serverPort;
    }

    @Test
    public void testRegisterUser(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue());
        final List<User> users = userRepository.getAll();
        Assert.assertEquals("User repository must contain 1 user", 1, users.size());
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 1 session", 1, sessions.size());
    }

    @Test
    public void testRegisterUserTwice(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        userRepository.create(user);
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testRegisterTooShortUsername(){
        final User user = new User("us", "password", "artdippel@mail.ru");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testRegisterTooShortPassword(){
        final User user = new User("username1", "pas", "artdippel@mail.ru");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

       public void testRegisterWrongEmail(){
        final User user = new User("username1", "password", "artdippelu");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }
    @Test
    public void testRegisterNullUsername(){
        final User user = new User(null, "password", "artdippelu");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testUnregister(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        final UserLoginRequest userLoginRequest = new UserLoginRequest("username1", "password");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue());
        List<User> users = userRepository.getAll();
        List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());
        Assert.assertEquals("Session repository must contain 1 sessions", 1, sessions.size());

        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().delete("/api/user")
                .then().statusCode(HttpStatus.SC_OK);
        users = userRepository.getAll();
        sessions = sessionRepository.getAll();
        Assert.assertEquals("User repository must contain 0 user", 0, users.size());
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
    }

    @Test
       public void testUnregisterWithWrongUserName(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue());
        List<User> users = userRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());

        final UserLoginRequest userLoginRequest = new UserLoginRequest("userna", "password");
        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().delete("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        users = userRepository.getAll();
        Assert.assertEquals("User repository must contain 1 user", 1, users.size());
    }

    @Test
    public void testUnregisterWithWrongPassword(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue());
        List<User> users = userRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());

        final UserLoginRequest userLoginRequest = new UserLoginRequest("username1", "passw");
        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().delete("/api/user")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        users = userRepository.getAll();
        Assert.assertEquals("User repository must contain 1 user", 1, users.size());
    }

    @Test
    public void testLogout(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        final String token = RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue()).extract().path("token");
        List<User> users = userRepository.getAll();
        List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());
        Assert.assertEquals("Session repository must contain 1 sessions", 1, sessions.size());

        RestAssured.given().header("token", token)
                .when().delete("/api/session")
                .then().statusCode(HttpStatus.SC_OK);
        users = userRepository.getAll();
        sessions = sessionRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
    }

    @Test
    public void testLogoutWithWrongToken(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        final String token = RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue()).extract().path("token");
        final List<User> users = userRepository.getAll();
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());
        Assert.assertEquals("Session repository must contain 1 sessions", 1, sessions.size());

        RestAssured.given().header("token", token+"wrong")
                .when().delete("/api/session")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testLogoutWithNullToken(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        final String token = RestAssured.given().request()
                .body(user).contentType(ContentType.JSON)
                .when().post("/api/user")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue()).extract().path("token");
        final List<User> users = userRepository.getAll();
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User repository must contain 1 users", 1, users.size());
        Assert.assertEquals("Session repository must contain 1 sessions", 1, sessions.size());
        RestAssured.given()
                .when().delete("/api/session")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testLogin(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        userRepository.create(user);
        final UserLoginRequest userLoginRequest = new UserLoginRequest("username1", "password");
        List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().post("/api/session")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue());
        sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 1 sessions", 1, sessions.size());
    }

    @Test
    public void testLoginTwice(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        userRepository.create(user);
        final UserLoginRequest userLoginRequest = new UserLoginRequest("username1", "password");
        List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().post("/api/session")
                .then().statusCode(HttpStatus.SC_CREATED)
                .body("token", Matchers.notNullValue());
        sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 1 sessions", 1, sessions.size());

        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().post("/api/session")
                .then().statusCode(HttpStatus.SC_CREATED);
        sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must still contain 1 sessions", 1, sessions.size());
    }

    @Test
    public void testLoginWithWrongUserName(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        userRepository.create(user);
        final UserLoginRequest userLoginRequest = new UserLoginRequest("username3", "password");
        List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().post("/api/session")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
    }

    @Test
    public void testLoginWithWrongPassword(){
        final User user = new User("username1", "password", "artdippel@mail.ru");
        userRepository.create(user);
        final UserLoginRequest userLoginRequest = new UserLoginRequest("username1", "password5");
        List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
        RestAssured.given().request()
                .body(userLoginRequest).contentType(ContentType.JSON)
                .when().post("/api/session")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST);
        sessions = sessionRepository.getAll();
        Assert.assertEquals("Session repository must contain 0 sessions", 0, sessions.size());
    }
}