package net.thumbtack.dippel.siteupdatenotifier.utils;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.Page;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by adippel on 09.09.2015.
 */
public class PageUtil {
    private static String url1 = "http://yandex.ru.com/";
    private static String category1 = "fun";

    private static String url2 = "https://docs.oracle.com/javase/tutorial/networking/urls/creatingUrls.html";
    private static String category2 = "doc";

    public static Page createPage1(){
        try {
            return new Page(new URL(url1), category1);
        }catch (Exception e){}
        return null;
    }

    public static Page createPage2(){
        try {
            return new Page(new URL(url2), category2);
        }catch (Exception e){}
        return null;
    }

    public static URL getUrl1() {
        try {
            return new URL(url1);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCategory1() {
        return category1;
    }

    public static String getUrl2() {
        return url2;
    }

    public static String getCategory2() {
        return category2;
    }
}
