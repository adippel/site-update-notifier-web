package net.thumbtack.dippel.siteupdatenotifier.utils;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;

/**
 * Created by adippel on 08.09.2015.
 */
public class UserUtil {
    private static String username = "usernameTest";
    private static String password ="passwordTest";
    private static String mail = "artdippel@mail.ru";

    public static User createUser(){
        return new User(username, password, mail);
    }
    public static User createUser(String userName){
        return new User(userName, password, mail);
    }

}
