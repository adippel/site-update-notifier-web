package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.SiteUpdateNotifierServer;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.PageResponse;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;
import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserPageDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import net.thumbtack.dippel.siteupdatenotifier.utils.PageUtil;
import net.thumbtack.dippel.siteupdatenotifier.utils.UserUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by adippel on 09.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SiteUpdateNotifierServer.class)
@Transactional
public class PageServiceImplTest {


    @Autowired
    PageService pageService;
    @Autowired
    SessionService sessionService;
    @Autowired
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Autowired
    @Qualifier("primaryUserPageRepository")
    private UserPageDAO userPageRepository;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        sessionRepository.deleteAll();
        userPageRepository.deleteAll();
    }

    @Test
    public void testAddPage(){
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("UserPage shouldnt be in repository", 0, userPages.size());
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        userPages = userPageRepository.getAll();
        Assert.assertEquals("UserPage should be in repository", 1, userPages.size());
    }

    @Test(expected = SiteUpdateException.class)
    public void testAddPageTwice(){
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        pageService.addPage(token, PageUtil.createPage1());
    }

    @Test(expected = SiteUpdateException.class)
    public void testAddWithWrongToken(){
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token+"wrong", PageUtil.createPage1());
    }

    @Test
    public void testGetPages(){
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        pageService.addPage(token, PageUtil.createPage2());
        final List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("2 UserPages should be in repository", 2, userPages.size());
        final List<PageResponse> pageResponses = pageService.getPages(token);
        Assert.assertEquals("2 PageResponces should be returned", 2, pageResponses.size());
    }

    @Test(expected = SiteUpdateException.class)
    public void testGetPagesWithWrongToken(){
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        pageService.addPage(token, PageUtil.createPage2());
        final List<PageResponse> pageResponses = pageService.getPages(token+"wrong");
    }

    @Test
    public void testGetPagesWhenNoPagesExist(){
        final String token = sessionService.register(UserUtil.createUser());
        final List<PageResponse> pageResponses = pageService.getPages(token);
        Assert.assertNull(pageResponses);
    }

    @Test
    public void testDeletePage(){
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        pageService.addPage(token, PageUtil.createPage2());
        List<UserPage> userPages = userPageRepository.getAll();
        Assert.assertEquals("2 UserPages should be in repository", 2, userPages.size());
        pageService.deletePage(token, PageUtil.getUrl1());
        userPages = userPageRepository.getAll();
        Assert.assertEquals("2 UserPages should be in repository", 1, userPages.size());
        Assert.assertEquals("Wrong URL is stored in userpage", PageUtil.getUrl2(), userPages.get(0).getUrl().toString());
    }

    @Test(expected = SiteUpdateException.class)
    public void testDeletePageWithWrongToken(){
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        pageService.deletePage(token + "wrong", PageUtil.getUrl1());
    }

    @Test(expected = SiteUpdateException.class)
    public void testDeletePageWithWrongURL() throws MalformedURLException {
        final String token = sessionService.register(UserUtil.createUser());
        pageService.addPage(token, PageUtil.createPage1());
        pageService.deletePage(token, new URL("https://www.yandex.ru/"));
    }


    @Test(expected = SiteUpdateException.class)
    public void testDeleteOtherUserPage(){
        final String token = sessionService.register(UserUtil.createUser());
        final String token1 = sessionService.register(UserUtil.createUser("otherUser"));
        pageService.addPage(token, PageUtil.createPage1());
        pageService.deletePage(token1, PageUtil.getUrl1());
    }








}