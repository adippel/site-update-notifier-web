package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.SiteUpdateNotifierServer;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;
import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserPageDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import net.thumbtack.dippel.siteupdatenotifier.utils.UserUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by adippel on 08.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SiteUpdateNotifierServer.class)
@Transactional
public class SessionServiceImplTest {
    @Autowired
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Autowired
    @Qualifier("primaryUserPageRepository")
    private UserPageDAO userPageRepository;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;

    @Autowired
    private SessionService sessionService;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        sessionRepository.deleteAll();
        userPageRepository.deleteAll();
    }

    @After
    public void clear() throws Exception{
    }

    @Test
    public void testRegister() {
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        Assert.assertNotNull(token);
        final List<User> users = userRepository.getAll();
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User should be in repository", 1, users.size());
        Assert.assertEquals("User session should be in repository", 1, sessions.size());
    }

    @Test(expected = SiteUpdateException.class)
    public void testRegisterTwice() {
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.register(user);
    }

    @Test
    public void testUnregister() {
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.unregister(user.getUsername(), user.getPassword());
        final List<User> users = userRepository.getAll();
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User shouldnt be in repository", 0, users.size());
        Assert.assertEquals("User session shouldnt be in repository", 0, sessions.size());
    }

    @Test(expected = SiteUpdateException.class)
    public void testUnregisterTwice() {
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.unregister(user.getUsername(), user.getPassword());
        sessionService.unregister(user.getUsername(), user.getPassword());
    }

    @Test(expected = SiteUpdateException.class)
    public void testUnregisterWithWrongPassword(){
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.unregister(user.getUsername(), user.getPassword()+"wrong");
    }

    @Test(expected = SiteUpdateException.class)
    public void testUnregisterWithWrongUsername(){
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.unregister(user.getUsername()+"wrong", user.getPassword());
    }

    @Test
    public void testLogout(){
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.logout(token);
        final List<User> users = userRepository.getAll();
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User should be in repository", 1, users.size());
        Assert.assertEquals("User session shouldnt be in repository", 0, sessions.size());
    }

    @Test(expected = SiteUpdateException.class)
    public void testLogoutTwice(){
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.logout(token);
        sessionService.logout(token);
    }

    @Test(expected = SiteUpdateException.class)
    public void testLogoutWithWrongToken(){
        final User user = UserUtil.createUser();
        final String token = sessionService.register(user);
        sessionService.logout(token+"wrong");
    }

    @Test
    public void testLogin() {
        final User user = UserUtil.createUser();
        String token = sessionService.register(user);
        sessionService.logout(token);
        token = sessionService.login(user.getUsername(), user.getPassword());
        Assert.assertNotNull(token);
        final List<User> users = userRepository.getAll();
        final List<Session> sessions = sessionRepository.getAll();
        Assert.assertEquals("User should be in repository", 1, users.size());
        Assert.assertEquals("User session should be in repository", 1, sessions.size());
    }

    @Test
    public void testLoginTwice() {
        final User user = UserUtil.createUser();
        String token = sessionService.register(user);
        sessionService.logout(token);
        token = sessionService.login(user.getUsername(), user.getPassword());
        token = sessionService.login(user.getUsername(), user.getPassword());
    }

    @Test(expected = SiteUpdateException.class)
    public void testLoginWithWrongUsername() {
        final User user = UserUtil.createUser();
        String token = sessionService.register(user);
        sessionService.logout(token);
        token = sessionService.login(user.getUsername()+"wrong", user.getPassword());
    }

    @Test(expected = SiteUpdateException.class)
    public void testLoginWithWrongPassword() {
        final User user = UserUtil.createUser();
        String token = sessionService.register(user);
        sessionService.logout(token);
        token = sessionService.login(user.getUsername(), user.getPassword()+"wrong");
    }




}