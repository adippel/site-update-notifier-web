package net.thumbtack.dippel.siteupdatenotifier.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by adippel on 14.09.2015.
 */
@Configuration
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${mongo.db.name}")
    private String name;

    @Value("${mongo.db.host}")
    private String host;

    @Value("${mongo.db.port}")
    private int port;


    @Override
    protected String getDatabaseName() {
        return name;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(host, port);
    }
}
