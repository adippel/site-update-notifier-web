package net.thumbtack.dippel.siteupdatenotifier.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by adippel on 25.09.2015.
 */
@Configuration
public class SchedulerConfig implements SchedulingConfigurer {

    @Value("${scheduler.threadPoolSize}")
    private int threadPoolSize;

    @Override
    public void configureTasks(final ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.setScheduler(taskExecutor());
    }

    public Executor taskExecutor() {
        return Executors.newScheduledThreadPool(threadPoolSize);
    }
}
