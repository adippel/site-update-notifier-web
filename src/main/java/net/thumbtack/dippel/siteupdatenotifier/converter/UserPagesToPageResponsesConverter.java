package net.thumbtack.dippel.siteupdatenotifier.converter;

import net.thumbtack.dippel.siteupdatenotifier.clientmodels.PageResponse;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adippel on 23.09.2015.
 */
@Component
public class UserPagesToPageResponsesConverter implements Converter<List<UserPage>, List<PageResponse>> {
    @Override
    public List<PageResponse> convert(List<UserPage> userPages) {
        final List<PageResponse> pageResponses = new ArrayList<PageResponse>();
        for(final UserPage userPage: userPages){
            final URL url = userPage.getUrl();
            final String category = userPage.getCategory();
            final Boolean isChanged = userPage.isChanged();
            final PageResponse pageResponse = new PageResponse(url, category, isChanged);
            pageResponses.add(pageResponse);
        }
        return pageResponses;
    }
}
