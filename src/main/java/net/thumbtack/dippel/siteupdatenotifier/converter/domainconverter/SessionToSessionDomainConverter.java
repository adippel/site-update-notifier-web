package net.thumbtack.dippel.siteupdatenotifier.converter.domainconverter;

import net.thumbtack.dippel.siteupdatenotifier.domains.SessionDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 16.09.2015.
 */
@Component
public class SessionToSessionDomainConverter implements Converter<Session,SessionDomain> {
    @Override
    public SessionDomain convert(final Session session) {
        final SessionDomain sessionDomain = new SessionDomain();
        sessionDomain.setUsername(session.getUsername());
        sessionDomain.setToken(session.getToken());
        sessionDomain.setCreationTime(session.getCreationTime());
        return sessionDomain;
    }
}
