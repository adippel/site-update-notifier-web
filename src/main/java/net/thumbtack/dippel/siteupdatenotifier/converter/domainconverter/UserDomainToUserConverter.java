package net.thumbtack.dippel.siteupdatenotifier.converter.domainconverter;

import net.thumbtack.dippel.siteupdatenotifier.domains.UserDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 16.09.2015.
 */
@Component
public class UserDomainToUserConverter implements Converter<UserDomain, User> {
    @Override
    public User convert(final UserDomain userDomain) {
        final User user = new User();
        System.out.println(userDomain);
        user.setUsername(userDomain.getUsername());
        user.setPassword(userDomain.getPassword());
        user.setMail(userDomain.getMail());
        return user;
    }
}
