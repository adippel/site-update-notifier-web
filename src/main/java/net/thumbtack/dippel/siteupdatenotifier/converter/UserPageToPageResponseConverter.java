package net.thumbtack.dippel.siteupdatenotifier.converter;

import net.thumbtack.dippel.siteupdatenotifier.clientmodels.PageResponse;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 15.09.2015.
 */
@Component
public class UserPageToPageResponseConverter implements Converter<UserPage, PageResponse> {

    @Override
    public PageResponse convert(final UserPage userPage){
        return new PageResponse(userPage.getUrl(),userPage.getCategory(),userPage.isChanged());
    }
}