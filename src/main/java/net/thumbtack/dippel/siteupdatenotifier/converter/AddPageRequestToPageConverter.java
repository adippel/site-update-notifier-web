package net.thumbtack.dippel.siteupdatenotifier.converter;

import net.thumbtack.dippel.siteupdatenotifier.clientmodels.AddPageRequest;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Page;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 15.09.2015.
 */
@Component
public class AddPageRequestToPageConverter implements Converter<AddPageRequest,Page> {
    @Override
    public Page convert(final AddPageRequest addPageRequest) {
        return new Page(addPageRequest.getUrl(), addPageRequest.getCategory());
    }
}
