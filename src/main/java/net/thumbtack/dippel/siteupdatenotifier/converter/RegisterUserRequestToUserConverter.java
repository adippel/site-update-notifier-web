package net.thumbtack.dippel.siteupdatenotifier.converter;

import net.thumbtack.dippel.siteupdatenotifier.clientmodels.RegisterUserRequest;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 15.09.2015.
 */
@Component
public class RegisterUserRequestToUserConverter implements Converter<RegisterUserRequest, User> {
    @Override
    public User convert(final RegisterUserRequest registerUserRequest) {
        return new User(registerUserRequest.getUsername(), registerUserRequest.getPassword(),registerUserRequest.getMail());
    }
}
