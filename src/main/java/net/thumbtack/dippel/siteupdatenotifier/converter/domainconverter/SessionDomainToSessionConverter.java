package net.thumbtack.dippel.siteupdatenotifier.converter.domainconverter;

import net.thumbtack.dippel.siteupdatenotifier.domains.SessionDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 16.09.2015.
 */
@Component
public class SessionDomainToSessionConverter implements Converter<SessionDomain, Session> {
    @Override
    public Session convert(final SessionDomain sessionDomain) {
        final Session session = new Session();
        session.setToken(sessionDomain.getToken());
        session.setUsername(sessionDomain.getUsername());
        session.setCreationTime(sessionDomain.getCreationTime());
        return session;
    }
}
