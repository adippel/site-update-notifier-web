package net.thumbtack.dippel.siteupdatenotifier.converter.domainconverter;

import net.thumbtack.dippel.siteupdatenotifier.domains.UserPageDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 16.09.2015.
 */
@Component
public class UserPageToUserPageDomain implements Converter<UserPage, UserPageDomain> {
    @Override
    public UserPageDomain convert(UserPage userPage) {
        final UserPageDomain userPageDomain = new UserPageDomain();
        userPageDomain.setUsername(userPage.getUsername());
        userPageDomain.setUserPageId(userPage.getUserPageId());
        userPageDomain.setDocHash(userPage.getDocHash());
        userPageDomain.setChanged(userPage.isChanged());
        userPageDomain.setCategory(userPage.getCategory());
        userPageDomain.setUrl(userPage.getUrl());
        userPageDomain.setTextContent(userPage.getTextContent());
        return userPageDomain;
    }
}
