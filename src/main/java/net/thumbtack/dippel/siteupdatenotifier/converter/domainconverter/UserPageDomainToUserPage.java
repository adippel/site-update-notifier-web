package net.thumbtack.dippel.siteupdatenotifier.converter.domainconverter;

import net.thumbtack.dippel.siteupdatenotifier.domains.UserPageDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 16.09.2015.
 */
@Component
public class UserPageDomainToUserPage implements Converter<UserPageDomain, UserPage>{
    @Override
    public UserPage convert(final UserPageDomain userPageDomain) {
        final UserPage userPage = new UserPage();
        userPage.setUserPageId(userPageDomain.getUserPageId());
        userPage.setUrl(userPageDomain.getUrl());
        userPage.setCategory(userPageDomain.getCategory());
        userPage.setUsername(userPageDomain.getUsername());
        userPage.setChanged(userPageDomain.isChanged());
        userPage.setDocHash(userPageDomain.getDocHash());
        userPage.setTextContent(userPageDomain.getTextContent());
        return userPage;
    }
}
