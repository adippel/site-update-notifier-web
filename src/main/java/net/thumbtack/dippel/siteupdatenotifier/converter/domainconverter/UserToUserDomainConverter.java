package net.thumbtack.dippel.siteupdatenotifier.converter.domainconverter;

import net.thumbtack.dippel.siteupdatenotifier.domains.UserDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 16.09.2015.
 */
@Component
public class UserToUserDomainConverter implements Converter<User, UserDomain> {
    @Override
    public UserDomain convert(final User user) {
        final UserDomain userDomain = new UserDomain();
        userDomain.setUsername(user.getUsername());
        userDomain.setPassword(user.getPassword());
        userDomain.setMail(user.getMail());
        return userDomain;
    }
}
