package net.thumbtack.dippel.siteupdatenotifier.mappers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.RegisterUserRequest;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;

import java.io.IOException;

/**
 * Created by adippel on 15.09.2015.
 */
public class RegisterUserRequestMapper extends JsonDeserializer<RegisterUserRequest> {
    @Override
    public RegisterUserRequest deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) {

        final ObjectCodec objectCodec = jsonParser.getCodec();
        final JsonNode jsonNode;
        try {
            jsonNode = objectCodec.readTree(jsonParser);
            final RegisterUserRequest registerUserRequest = new RegisterUserRequest();
            registerUserRequest.setUsername(jsonNode.get("username").asText());
            registerUserRequest.setPassword(jsonNode.get("password").asText());
            registerUserRequest.setMail(jsonNode.get("mail").asText());
            return registerUserRequest;
        } catch (final IOException e) {
            throw new SiteUpdateException("Deserialization error");
        }
    }
}
