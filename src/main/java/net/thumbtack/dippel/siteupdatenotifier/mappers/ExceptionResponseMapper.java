package net.thumbtack.dippel.siteupdatenotifier.mappers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.ExceptionResponse;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;

import java.io.IOException;

/**
 * Created by adippel on 23.09.2015.
 */
public class ExceptionResponseMapper extends JsonSerializer<ExceptionResponse> {
    @Override
    public void serialize(ExceptionResponse exceptionResponse, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        try {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("message", exceptionResponse.getMessage());
            jsonGenerator.writeEndObject();
        } catch (final IOException e) {
            throw new SiteUpdateException("Serialization error");
        }
    }
}
