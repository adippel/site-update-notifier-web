package net.thumbtack.dippel.siteupdatenotifier.mappers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.AddPageRequest;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;

import java.io.IOException;
import java.net.URL;

/**
 * Created by adippel on 15.09.2015.
 */
public class AddPageRequestMapper extends JsonDeserializer<AddPageRequest> {
    @Override
    public AddPageRequest deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext){
        final ObjectCodec objectCodec = jsonParser.getCodec();
        final JsonNode jsonNode;
        try {
            jsonNode = objectCodec.readTree(jsonParser);
            System.out.println(jsonNode.toString());
            return new AddPageRequest(new URL(jsonNode.get("url").asText()),jsonNode.get("category").asText());
        } catch (final IOException e) {
            throw new SiteUpdateException("Deserialization error");
        }
    }
}
