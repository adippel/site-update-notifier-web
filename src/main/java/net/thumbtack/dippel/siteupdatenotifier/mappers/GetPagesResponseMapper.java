package net.thumbtack.dippel.siteupdatenotifier.mappers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.GetPagesResponse;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.PageResponse;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;

import java.io.IOException;

/**
 * Created by adippel on 15.09.2015.
 */
public class GetPagesResponseMapper extends JsonSerializer<GetPagesResponse> {
    @Override
    public void serialize(final GetPagesResponse getPagesResponse, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider){
        try {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeArrayFieldStart("pages");
            for(final PageResponse pageResponse:getPagesResponse.getPages()){
                jsonGenerator.writeObject(pageResponse);
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        } catch (final IOException e) {
           throw new SiteUpdateException("Serialization error");
        }

    }
}
