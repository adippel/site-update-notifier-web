package net.thumbtack.dippel.siteupdatenotifier;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@ImportResource("classpath:database.xml")
@PropertySource("application.properties")
public class SiteUpdateNotifierServer {
    private static final Logger logger = Logger.getLogger(SiteUpdateNotifierServer.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SiteUpdateNotifierServer.class, args);
        logger.warn("Server is started");
    }

}