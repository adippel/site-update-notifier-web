package net.thumbtack.dippel.siteupdatenotifier.servermodels;

/**
 * Created by adippel on 17.09.2015.
 */
public class PageContent {
    private int docHash;
    private String textContent;

    public PageContent(final int docHash, final String textContent) {
        this.docHash = docHash;
        this.textContent = textContent;
    }

    public PageContent() {
    }

    public int getDocHash() {
        return docHash;
    }

    public void setDocHash(final int docHash) {
        this.docHash = docHash;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(final String textContent) {
        this.textContent = textContent;
    }
}
