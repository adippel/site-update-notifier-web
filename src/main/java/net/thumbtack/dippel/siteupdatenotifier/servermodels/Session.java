package net.thumbtack.dippel.siteupdatenotifier.servermodels;

import java.util.Date;

/**
 * Created by adippel on 02.09.2015.
 */
public class Session {
    private String token;
    private String username;
    private Date creationTime;

    public Session(final String username) {
        this.username = username;
    }

    public Session() {
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Session{" +
                "token='" + token + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Session session = (Session) o;

        if (!token.equals(session.token)) return false;
        return username.equals(session.username);

    }

    @Override
    public int hashCode() {
        int result = token.hashCode();
        result = 31 * result + username.hashCode();
        return result;
    }
}
