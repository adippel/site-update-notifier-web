package net.thumbtack.dippel.siteupdatenotifier.servermodels;

import java.net.URL;

/**
 * Created by adippel on 04.09.2015.
 */
public class Page {
    private URL url;
    private String category;

    public Page(final URL url, final String category) {
        this.url = url;
        this.category = category;
    }

    public Page() {
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }
}
