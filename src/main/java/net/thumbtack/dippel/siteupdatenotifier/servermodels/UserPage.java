package net.thumbtack.dippel.siteupdatenotifier.servermodels;

import org.springframework.data.annotation.Id;

import java.net.URL;

/**
 * Created by adippel on 02.09.2015.
 */
public class UserPage {
    @Id
    private int userPageId;
    private URL url;
    private String category;
    private String username;
    private boolean isChanged;
    private Integer docHash;
    private String textContent;


    public UserPage(final Page page, final String username) {
        this.url = page.getUrl();
        this.category = page.getCategory();
        this.username = username;
        isChanged = false;
    }

    public UserPage() {
    }

    public int getUserPageId() {
        return userPageId;
    }

    public void setUserPageId(final int userPageId) {
        this.userPageId = userPageId;
    }

    public Integer getDocHash() {
        return docHash;
    }

    public void setDocHash(final Integer docHash) {
        this.docHash = docHash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(final boolean isChanged) {
        this.isChanged = isChanged;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(final String textContent) {
        this.textContent = textContent;
    }

    @Override
    public String toString() {
        return "UserPage{" +
                "userPageId='" + userPageId + '\'' +
                ", url=" + url +
                ", category='" + category + '\'' +
                ", username='" + username + '\'' +
                ", isChanged=" + isChanged +
                ", docHash=" + docHash +
                '}';
    }
}
