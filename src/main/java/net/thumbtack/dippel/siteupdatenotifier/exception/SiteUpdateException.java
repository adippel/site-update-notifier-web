package net.thumbtack.dippel.siteupdatenotifier.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by adippel on 02.09.2015.
 */

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class SiteUpdateException extends RuntimeException {

    public SiteUpdateException(){}

    public SiteUpdateException(final String message){
        super(message);
    }
}
