package net.thumbtack.dippel.siteupdatenotifier.repositories;


import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by adippel on 11.09.2015.
 */

@Repository("userJDBCTemplate")
public class UserJDBCTemplate implements UserDAO {

    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    public void setDataSource(final DataSource dataSource){
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public void create(final User user){
        final String SQL = "insert into user (username, password, mail) values(?, ?, ?)";
        jdbcTemplateObject.update(SQL, user.getUsername(), user.getPassword(), user.getMail());
    }

    @Override
    public User get(final String username){
        final String SQL = "select * from user where username = ?";
        final List<User> users = jdbcTemplateObject.query(SQL, new Object[]{username}, new UserMapper());
        if(users.isEmpty()) {
            return null;
        }else{
            return users.get(0);
        }
    }

    @Override
    public void delete(final String username){
        final String SQL = "delete from user where username = ?";
        jdbcTemplateObject.update(SQL, username);
    }

    @Override
    public void update(final User user){
        final String SQL = "update user set password = ?, mail = ? where username = ?";
        jdbcTemplateObject.update(SQL, user.getPassword(), user.getMail(), user.getUsername());
    }

    @Override
    public void deleteAll(){
        final String SQL = "delete from user";
        jdbcTemplateObject.update(SQL);
    }

    @Override
    public List<User> getAll(){
        final String SQL ="select * from user";
        return jdbcTemplateObject.query(SQL, new UserMapper());
    }

    static class UserMapper implements RowMapper<User> {
        public User mapRow(final ResultSet rs, final int rowNum) throws SQLException {
            final User user = new User();
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setMail(rs.getString("mail"));
            return user;
        }
    }
}
