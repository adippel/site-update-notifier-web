package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;

import java.util.List;

/**
 * Created by adippel on 11.09.2015.
 */
public interface UserDAO {

    //void setDataSource(DataSource ds);

    public void create(User user);

    public User get(String username);

    public void delete(String username);

    public void update(User user);

    public void deleteAll();

    public List<User> getAll();

}


