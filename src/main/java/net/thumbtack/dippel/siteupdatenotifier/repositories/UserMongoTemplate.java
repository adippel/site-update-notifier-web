package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.domains.UserDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adippel on 14.09.2015.
 */
@Repository("userMongoTemplate")
public class UserMongoTemplate implements UserDAO {

    @Autowired
    Converter<User,UserDomain> userUserDomainConverter;
    @Autowired
    Converter<UserDomain,User> userDomainUserConverter;

    @Autowired
    MongoTemplate mongoTemplate;

    private String userDocName = "userdomain";

    @Override
    public void create(final User user) {
        final UserDomain userDomain = userUserDomainConverter.convert(user);
        mongoTemplate.save(userDomain, userDocName);
    }

    @Override
    public User get(final String username) {
        final Query findUserByUsername = new Query(Criteria.where("username").is(username));
        final UserDomain userDomain = mongoTemplate.findOne(findUserByUsername, UserDomain.class, userDocName);
        if(userDomain == null){
            return null;
        }
        return userDomainUserConverter.convert(userDomain);
    }

    @Override
    public void delete(final String username) {
        final Query findUserByUsername = new Query(Criteria.where("username").is(username));
        mongoTemplate.remove(findUserByUsername, UserDomain.class, userDocName);
    }

    @Override
    public void update(final User user) {
        final UserDomain userDomain = userUserDomainConverter.convert(user);
        final Query findUserByUsername = new Query(Criteria.where("username").is(user.getUsername()));
        mongoTemplate.updateFirst(findUserByUsername, Update.update("password", userDomain.getPassword()), UserDomain.class, userDocName);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection(userDocName);
    }

    @Override
    public List<User> getAll() {
        final List<UserDomain> userDomains =  mongoTemplate.findAll(UserDomain.class, userDocName);
        final List<User> users = new ArrayList<>();
        for(final UserDomain userDomain : userDomains){
            users.add(userDomainUserConverter.convert(userDomain));
        }
        return users;
    }
}

