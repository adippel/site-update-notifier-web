package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by adippel on 11.09.2015.
 */
@Repository("sessionJDBCTemplate")
public class SessionJDBCTemplate implements SessionDAO {

    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    public void setDataSource(final DataSource dataSource){
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public void create(final Session session){
        session.setToken(UUID.randomUUID().toString());
        session.setCreationTime(new Date());
        final String SQL = "insert into session (token, username, creationtime) values(?, ?, ?)";
        jdbcTemplateObject.update(SQL, session.getToken(), session.getUsername(), session.getCreationTime());
    }

    @Override
    public Session get(final String token){
        final String SQL = "select * from session where token = ?";
        final List<Session> sessions = jdbcTemplateObject.query(SQL, new Object[]{token},new SessionMapper());
        if(sessions.isEmpty()) {
            return null;
        }else{
            return sessions.get(0);
        }
    }

    @Override
    public void delete(final String token){
        final String SQL = "delete from session where token = ?";
        jdbcTemplateObject.update(SQL, token);
    }

    @Override
    public void update(final Session session){
    }

    @Override
    public Session findByUsername(final String username){
        final String SQL = "select * from session where username = ?";
        final List<Session> sessions = jdbcTemplateObject.query(SQL, new Object[]{username}, new SessionMapper());
        if(sessions.isEmpty()) {
            return null;
        }else{
            return sessions.get(0);
        }
    }

    @Override
    public void deleteAll(){
        final String SQL = "delete from session";
        jdbcTemplateObject.update(SQL);
    }

    @Override
    public List<Session> getAll(){
        final String SQL ="select * from session";
        final List<Session> sessions = jdbcTemplateObject.query(SQL, new SessionMapper());
        return sessions;
    }

    @Override
    public void deleteWhereCreationtimeIsEarlier(final Date date) {
        final String SQL = "delete from session where creationtime < ?";
        jdbcTemplateObject.update(SQL, date);
    }

    static class SessionMapper implements RowMapper<Session> {
        public Session mapRow(final ResultSet rs, final int rowNum) throws SQLException {
            final Session session = new Session();
            session.setToken(rs.getString("token"));
            session.setUsername(rs.getString("username"));
            session.setCreationTime(rs.getDate("creationtime"));
            return session;
        }
    }
}
