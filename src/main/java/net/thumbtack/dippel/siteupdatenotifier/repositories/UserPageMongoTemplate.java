package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.domains.UserPageDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adippel on 14.09.2015.
 */
@Repository("userPageMongoTemplate")
public class UserPageMongoTemplate implements UserPageDAO {

    private static int counter;

    @Autowired
    Converter<UserPage, UserPageDomain> userPageUserPageDomainConverter;
    @Autowired
    Converter<UserPageDomain,UserPage> userPageDomainUserPageConverter;
    @Autowired
    MongoTemplate mongoTemplate;

    private String userPageDocName = "userpagedomain";

    @Override
    public void create(final UserPage userPage) {
        counter++;
        userPage.setUserPageId(counter);
        final UserPageDomain userPageDomain = userPageUserPageDomainConverter.convert(userPage);
        mongoTemplate.insert(userPageDomain, userPageDocName);
    }

    @Override
    public UserPage get(final int userPageId) {
        final Query findUserPageByUserPageId = new Query(Criteria.where("userPageId").is(userPageId));
        final UserPageDomain userPageDomain = mongoTemplate.findOne(findUserPageByUserPageId, UserPageDomain.class, userPageDocName);
        if(userPageDomain==null){
            return null;
        }
        return userPageDomainUserPageConverter.convert(userPageDomain);
    }

    @Override
    public void delete(final int userPageId) {
        final Query findUserPageByUserPageId = new Query(Criteria.where("userPageId").is(userPageId));
        mongoTemplate.remove(findUserPageByUserPageId, UserPageDomain.class, userPageDocName);
    }

    @Override
    public void update(final UserPage userPage) {
        final UserPageDomain userPageDomain = userPageUserPageDomainConverter.convert(userPage);
        final Query findUserPageByUserPageId = new Query(Criteria.where("userPageId").is(userPageDomain.getUserPageId()));
        final Update update = new Update().set("isChanged", userPageDomain.isChanged()).set("url", userPageDomain.getUrl()).set("textContent", userPageDomain.getTextContent());
        mongoTemplate.updateFirst(findUserPageByUserPageId, update, UserPageDomain.class, userPageDocName);
    }

    @Override
    public List<UserPage> findByUsername(final String username) {
        final Query findUserPagesByUsername = new Query(Criteria.where("username").is(username));
        final List<UserPageDomain> userPageDomains = mongoTemplate.find(findUserPagesByUsername, UserPageDomain.class, userPageDocName);
        if(userPageDomains==null){
            return null;
        }
        final List<UserPage> userPages = new ArrayList<>();
        for(final UserPageDomain userPageDomain : userPageDomains){
            userPages.add(userPageDomainUserPageConverter.convert(userPageDomain));
        }
        return userPages;
    }

    @Override
    public UserPage findByUsernameAndUrl(final String username, final URL url) {
        final Query findUserPageByUsernameAndUrl = new Query(Criteria.where("username").is(username).andOperator(Criteria.where("url").is(url)));
        final UserPageDomain userPageDomain = mongoTemplate.findOne(findUserPageByUsernameAndUrl, UserPageDomain.class, userPageDocName);
        if(userPageDomain==null){
            return null;
        }
        return userPageDomainUserPageConverter.convert(userPageDomain);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection(userPageDocName);
    }

    @Override
    public List<UserPage> getAll() {
        final List<UserPageDomain> userPageDomains =  mongoTemplate.findAll(UserPageDomain.class, userPageDocName);
        final List<UserPage> userPages = new ArrayList<>();
        for(final UserPageDomain userPageDomain : userPageDomains){
            userPages.add(userPageDomainUserPageConverter.convert(userPageDomain));
        }
        return userPages;
    }
}
