package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;

import java.net.URL;
import java.util.List;

/**
 * Created by adippel on 11.09.2015.
 */

public interface UserPageDAO {

    //void setDataSource(DataSource ds);

    public void create(UserPage userPage);

    public UserPage get(int userPageId);

    public void delete(int userPageId);

    public void update(UserPage userPage);

    public List<UserPage> findByUsername(String username);

    public UserPage findByUsernameAndUrl(String username, URL url);

    public void deleteAll();

    public List<UserPage> getAll();

}
