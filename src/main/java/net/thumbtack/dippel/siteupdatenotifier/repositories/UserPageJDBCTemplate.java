package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by adippel on 11.09.2015.
 */

@Repository("userPageJDBCTemplate")
public class UserPageJDBCTemplate implements UserPageDAO{

    private static final Logger logger = Logger.getLogger(UserPageJDBCTemplate.class);
    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    public void setDataSource(final DataSource dataSource){
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public void create(final UserPage userPage){
        final String SQL = "insert into userpage (url, category, username, ischanged, dochash, textcontent) values(?, ?, ?, ?, ?, ?)";
        jdbcTemplateObject.update(SQL, userPage.getUrl().toString(), userPage.getCategory(),
                userPage.getUsername(), userPage.isChanged(), userPage.getDocHash(), userPage.getTextContent());
    }

    @Override
    public UserPage get(final int userPageId){
        final String SQL = "select * from userpage where userpageid = ?";
        final List<UserPage> userPages = jdbcTemplateObject.query(SQL, new Object[]{userPageId}, new UserPageMapper());
        if(userPages.isEmpty()) {
            return null;
        }else{
            return userPages.get(0);
        }
    }

    @Override
    public void delete(final int userPageId){
        final String SQL = "delete from userpage where userpageid = ?";
        jdbcTemplateObject.update(SQL, userPageId);
    }

    @Override
    public void update(final UserPage userPage){
        final String SQL = "update userpage set ischanged = ?, dochash = ?, textcontent = ? where userpageid = ?";
        jdbcTemplateObject.update(SQL, userPage.isChanged(), userPage.getDocHash(), userPage.getTextContent(), userPage.getUserPageId());
    }

    @Override
    public List<UserPage> findByUsername(final String username){
        final String SQL = "select * from userpage where username = ?";
        final List<UserPage> userPages = jdbcTemplateObject.query(SQL, new Object[]{username}, new UserPageMapper());
        return userPages;
    }

    @Override
    public UserPage findByUsernameAndUrl(final String username, final URL url){
        final String SQL = "select * from userpage where username = ? and url = ?";
        final List<UserPage> userPages = jdbcTemplateObject.query(SQL, new Object[]{username, url.toString()}, new UserPageMapper());
        if(userPages.isEmpty()) {
            return null;
        }else{
            return userPages.get(0);
        }
    }

    @Override
    public void deleteAll(){
        final String SQL = "delete from userpage";
        jdbcTemplateObject.update(SQL);
    }

    @Override
    public List<UserPage> getAll(){
        final String SQL ="select * from userpage";
        final List<UserPage> userPages = jdbcTemplateObject.query(SQL, new UserPageMapper());
        return userPages;
    }

    static class UserPageMapper implements RowMapper<UserPage> {
        public UserPage mapRow(final ResultSet rs, final int rowNum) throws SQLException {
            final UserPage userPage = new UserPage();
            userPage.setUserPageId(rs.getInt("userpageid"));
            try {
                userPage.setUrl(new URL(rs.getString("url")));
            } catch (final MalformedURLException e) {
                e.printStackTrace();
            }
            userPage.setUsername(rs.getString("username"));
            userPage.setChanged(rs.getBoolean("ischanged"));
            userPage.setDocHash(rs.getInt("dochash"));
            userPage.setCategory(rs.getString("category"));
            userPage.setTextContent(rs.getString("textcontent"));
            return userPage;
        }
    }
}
