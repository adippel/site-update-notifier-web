package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.domains.SessionDomain;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by adippel on 14.09.2015.
 */
@Repository("sessionMongoTemplate")
public class SessionMongoTemplate implements SessionDAO {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    Converter<Session,SessionDomain> sessionSessionDomainConverter;
    @Autowired
    Converter<SessionDomain,Session> sessionDomainSessionConverter;

    private String sessionDocName = "sessiondomain";

    @Override
    public void create(final Session session) {
        session.setToken(UUID.randomUUID().toString());
        session.setCreationTime(new Date());
        final SessionDomain sessionDomain = sessionSessionDomainConverter.convert(session);
        System.out.println(sessionDomain);
        mongoTemplate.save(sessionDomain, sessionDocName);
    }

    @Override
    public Session get(final String token) {
        final Query findSessionByToken = new Query(Criteria.where("token").is(token));
        final SessionDomain sessionDomain = mongoTemplate.findOne(findSessionByToken, SessionDomain.class, sessionDocName);
        if(sessionDomain==null){
            return null;
        }
        return sessionDomainSessionConverter.convert(sessionDomain);
    }

    @Override
    public void delete(final String token) {
        final Query findSessionByToken = new Query(Criteria.where("token").is(token));
        mongoTemplate.remove(findSessionByToken, SessionDomain.class, sessionDocName);
    }

    @Override
    public void update(final Session session) {
    }

    @Override
    public Session findByUsername(final String username) {
        final Query findSessionByUsername = new Query(Criteria.where("username").is(username));
        final SessionDomain sessionDomain = mongoTemplate.findOne(findSessionByUsername, SessionDomain.class, sessionDocName);
        if(sessionDomain ==null){
            return null;
        }
        return sessionDomainSessionConverter.convert(sessionDomain);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection("sessiondomain");
    }

    @Override
    public List<Session> getAll() {
        final List<SessionDomain> sessionDomains = mongoTemplate.findAll(SessionDomain.class, sessionDocName);
        final List<Session> sessions = new ArrayList<>();
        for(final SessionDomain sessionDomain : sessionDomains){
            sessions.add(sessionDomainSessionConverter.convert(sessionDomain));
        }
        return sessions;
    }

    @Override
    public void deleteWhereCreationtimeIsEarlier(final Date date) {
        final Query creationTimeIsEarlier = new Query(Criteria.where("creationTime").lt(date));
        mongoTemplate.remove(creationTimeIsEarlier, SessionDomain.class, sessionDocName);
    }
}
