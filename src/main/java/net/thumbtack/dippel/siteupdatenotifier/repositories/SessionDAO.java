package net.thumbtack.dippel.siteupdatenotifier.repositories;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;

import java.util.Date;
import java.util.List;

/**
 * Created by adippel on 11.09.2015.
 */
public interface SessionDAO {

    //void setDataSource(DataSource ds);

    public void create(Session session);

    public Session get(String token);

    public void delete(String token);

    public void update(Session session);

    public Session findByUsername(String username);

    public void deleteAll();

    public List<Session> getAll();

    public void deleteWhereCreationtimeIsEarlier(Date date);
}
