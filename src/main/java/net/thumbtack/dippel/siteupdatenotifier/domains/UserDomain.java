package net.thumbtack.dippel.siteupdatenotifier.domains;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by adippel on 16.09.2015.
 */
@Document
public class UserDomain {

    private String username;
    private String password;
    private String mail;

    public UserDomain(final String name, final String password, final String mail) {
        this.username = name;
        this.mail = mail;
        this.password = password;
    }

    public UserDomain() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(final String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final UserDomain user = (UserDomain) o;

        if (!username.equals(user.username)) return false;
        return mail.equals(user.mail);

    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + mail.hashCode();
        return result;
    }
}
