package net.thumbtack.dippel.siteupdatenotifier.domains;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by adippel on 16.09.2015.
 */
@Document
public class SessionDomain {
    private String token;
    private String username;
    private Date creationTime;

    public SessionDomain(final String username) {
        this.username = username;
    }

    public SessionDomain() {
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "SessionDomain{" +
                "token='" + token + '\'' +
                ", username='" + username + '\'' +
                ", creationTime=" + creationTime +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final SessionDomain session = (SessionDomain) o;

        if (!token.equals(session.token)) return false;
        return username.equals(session.username);

    }

    @Override
    public int hashCode() {
        int result = token.hashCode();
        result = 31 * result + username.hashCode();
        return result;
    }

}
