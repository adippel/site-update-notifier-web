package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.DeletePageRequestMapper;

import java.net.URL;

/**
 * Created by adippel on 10.09.2015.
 */
@JsonDeserialize(using = DeletePageRequestMapper.class)
public class DeletePageRequest {
    private URL url;

    public DeletePageRequest(final URL url) {
        this.url = url;
    }

    public DeletePageRequest() {
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }
}

