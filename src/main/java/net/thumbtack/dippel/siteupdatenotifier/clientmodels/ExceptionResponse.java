package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.ExceptionResponseMapper;

/**
 * Created by adippel on 23.09.2015.
 */
@JsonSerialize(using = ExceptionResponseMapper.class)
public class ExceptionResponse {
    private String message;

    public ExceptionResponse(String message) {
        this.message = message;
    }

    public ExceptionResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
