package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.UserLoginRequestMapper;

import javax.validation.constraints.NotNull;

/**
 * Created by adippel on 02.09.2015.
 */
@JsonDeserialize(using = UserLoginRequestMapper.class)
public class UserLoginRequest {
    @NotNull
    String username;
    @NotNull
    String password;

    public UserLoginRequest(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    public UserLoginRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
