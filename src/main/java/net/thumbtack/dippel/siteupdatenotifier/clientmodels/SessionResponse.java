package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.SessionResponseMapper;

/**
 * Created by adippel on 02.09.2015.
 */
@JsonSerialize(using = SessionResponseMapper.class)
public class SessionResponse {
    private String token;

    public SessionResponse(final String token) {
        this.token = token;
    }

    public SessionResponse() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }
}
