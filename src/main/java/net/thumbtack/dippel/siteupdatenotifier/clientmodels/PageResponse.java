package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.PageResponseMapper;

import java.net.URL;

/**
 * Created by adippel on 02.09.2015.
 */
@JsonSerialize(using = PageResponseMapper.class)
public class PageResponse {
    private URL url;
    private boolean isChanged;
    private String category;

    public PageResponse(final URL url, final String category, final boolean isChanged) {
        this.url = url;
        this.category = category;
        this.isChanged = isChanged;
    }

    public PageResponse() {
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(final boolean isChanged) {
        this.isChanged = isChanged;
    }

    @Override
    public String toString() {
        return "PageResponse{" +
                "url=" + url +
                ", isChanged=" + isChanged +
                ", category='" + category + '\'' +
                '}';
    }
}
