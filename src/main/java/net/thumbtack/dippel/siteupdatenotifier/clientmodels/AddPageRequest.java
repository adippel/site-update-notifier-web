package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.AddPageRequestMapper;

import javax.validation.constraints.NotNull;
import java.net.URL;

/**
 * Created by adippel on 14.09.2015.
 */
@JsonDeserialize(using = AddPageRequestMapper.class)
public class AddPageRequest {
    @NotNull
    private URL url;
    @NotNull
    private String category;


    public AddPageRequest(final URL url, final String category) {
        this.url = url;
        this.category = category;
    }

    public AddPageRequest() {
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }
}
