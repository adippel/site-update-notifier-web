package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.GetPagesResponseMapper;

import java.util.List;

/**
 * Created by adippel on 14.09.2015.
 */
@JsonSerialize(using = GetPagesResponseMapper.class)
public class GetPagesResponse {
    private List<PageResponse> pages;

    public GetPagesResponse(final List<PageResponse> pages) {
        this.pages = pages;
    }

    public GetPagesResponse() {
    }

    public List<PageResponse> getPages() {
        return pages;
    }

    public void setPages(final List<PageResponse> pages) {
        this.pages = pages;
    }
}
