package net.thumbtack.dippel.siteupdatenotifier.clientmodels;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.thumbtack.dippel.siteupdatenotifier.mappers.RegisterUserRequestMapper;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by adippel on 14.09.2015.
 */
@JsonDeserialize(using = RegisterUserRequestMapper.class)
public class RegisterUserRequest {
    @NotNull
    @Size(min = 6 ,max = 30)
    private String username;
    @NotNull
    @Size(min = 6 ,max = 30)
    private String password;
    @NotNull
    private String mail;

    public RegisterUserRequest(final String name, final String password, final String mail) {
        this.username = name;
        this.mail = mail;
        this.password = password;
    }

    public RegisterUserRequest() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(final String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final RegisterUserRequest user = (RegisterUserRequest) o;

        if (!username.equals(user.username)) return false;
        return mail.equals(user.mail);

    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + mail.hashCode();
        return result;
    }
}
