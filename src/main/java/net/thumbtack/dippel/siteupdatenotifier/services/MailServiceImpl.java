package net.thumbtack.dippel.siteupdatenotifier.services;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

/**
 * Created by adippel on 02.09.2015.
 * This class consists of methods to operate with emails.
 * Methods of this class may catch MessagingException, MailException, IOException exceptions and log them.
 * <p>The methods of this class all throw a <tt>NullPointerException</tt>
 */
@Service
public class MailServiceImpl implements MailService  {

    private static final Logger logger = Logger.getLogger(MailServiceImpl.class);
    ApplicationContext context = new ClassPathXmlApplicationContext("springmail.xml");
    private JavaMailSender javaMailSender = (JavaMailSender) context.getBean("mailSender");

    /**
     *This method sends email to user with notify that page was changed and diffs html attachment.
     * @param mail string email of the destination.
     * @param username string name of the deatination user.
     * @param url url of the changed page.
     * @param diff html string with diff between old and current versions of page
     */
   @Override
   @Async
    public void sendMail(final String mail, final String username, final URL url, final String diff ) {
        final MimeMessage message = javaMailSender.createMimeMessage();
        final MimeMessageHelper helper;
       logger.warn("Sending mail - " + Thread.currentThread().getName());
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setSubject("SiteNotifier");
            helper.setTo(mail);
            helper.setText("<h4>Dear,  " + username + "! Resource <a href=\"" + url + "\"> " + url + "</a>  was changed</h4>.",true);
            final File attachment = createAttachment(diff);
            if (attachment != null) {
                helper.addAttachment(attachment.getName(), attachment);
            }
            javaMailSender.send(message);
            attachment.delete();
        } catch (final MessagingException ex) {
            logger.warn("Mail exception  " + ex.toString());
        } catch (final MailException ex) {
            logger.warn("Mail exception " + ex.toString());
        }
    }

    /*
    @Override
        public void sendMail(final String mail, final String username, final URL url, final String diff ) {
            final MimeMessage message = javaMailSender.createMimeMessage();
            try {
                message.setSubject("SiteNotifier");
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(mail));
                message.setContent("<h4>Dear,  " + username + "! Resource <a href=\"" + url + "\"> " + url + "</a>  was changed</h4>."+diff,"text/html; charset=utf-8");
                javaMailSender.send(message);
            } catch (final MessagingException ex) {
                logger.warn("Mail exception  " + ex.toString());
            } catch (final MailException ex) {
                logger.warn("Mail exception " + ex.toString());
            }
        }
        */
    /**
     *This method creates attachment file to be sent.
     * @param diff html string with diff between old and current versions of page
     */
    private File createAttachment(final String diff){
        try {
            final File tempFile = File.createTempFile("diff",".html");
            final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tempFile));
            bufferedWriter.write(diff);
            bufferedWriter.close();
            return tempFile;
        } catch (final IOException e) {
           logger.warn("Create attachment error");
            return null;
        }
    }


}
