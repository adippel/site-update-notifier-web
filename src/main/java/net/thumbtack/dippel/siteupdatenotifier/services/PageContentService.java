package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.PageContent;
import org.apache.log4j.Logger;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;

/**
 * Created by adippel on 17.09.2015.
 * This class consists of methods to operate with pages content,
 * like extracting content from web page and finding diffs between pages.
 * <p>All methods of this class all throw a <tt>NullPointerException</tt>
 */
@Component
public class PageContentService {

    private static final Logger logger = Logger.getLogger(PageContentService.class);
    private DiffMatchPatch diffMatchPatch = new DiffMatchPatch();

    /**
     * This method calculate diffs bettwen text content of pages. Method uses Diff Match Patch library
     * to find diffs and prepare html string with diff represantation.
     * @param oldText old version of page's text content
     * @param newText new version of page's text content
     * @return html string with diffs represantation
     */
    public String getHTMLDiff(final String oldText, final String newText){
        final LinkedList<DiffMatchPatch.Diff> deltas = diffMatchPatch.diffMain(oldText, newText);
        diffMatchPatch.diffCleanupSemantic(deltas);
        return diffMatchPatch.diffPrettyHtml(deltas);
    }

    /**
     * This method extracts page's text content and hash of text content.
     * Method uses Jsoup libary to parse data from web page.
     * @param url url of the web page to be parsed.
     * @return textContent - text content of web page, docHash - hash of text content
     *
     */
    public PageContent getPageContent(final URL url){
        try {
            final PageContent pageContent = new PageContent();
            final Document document = Jsoup.connect(url.toString()).get();
            final String docText = document.body().text();
            pageContent.setTextContent(docText);
            pageContent.setDocHash(docText.hashCode());
            return pageContent;
        }catch(final IOException ex) {
            logger.warn("Coudnt get DOM from url " + url);
            return null;
        }
    }
}
