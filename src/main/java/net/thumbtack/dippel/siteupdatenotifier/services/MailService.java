package net.thumbtack.dippel.siteupdatenotifier.services;

import java.net.URL;

/**
 * Created by adippel on 07.09.2015.
 */

public interface MailService {
    void sendMail(String mail, String username, URL url, String diff);
}
