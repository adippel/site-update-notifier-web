package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;
import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserPageDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by adippel on 02.09.2015.
 *
 * This class consists of methods to operate with User, Session models, their DAOs and provide data to user controller.
 * This class includes standart authorization methods: register/unregister, login/logout.
 *  <p>All methods of this class all throw a <tt>NullPointerException</tt>
 */

@Service
public class SessionServiceImpl implements SessionService {
    private static final Logger logger = Logger.getLogger(SessionServiceImpl.class);
    @Autowired
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Autowired
    @Qualifier("primaryUserPageRepository")
    private UserPageDAO userPageRepository;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;

    /**
     * This methods register user. User is added to userrepository, session is created and added to sessionRepository.
     * If user already exists method throws SiteUpdateException.
     * @param user user to be registered.
     * @return token identifier of session.
     */
    @Override
    public String register(final User user){
        final User existingUser = userRepository.get(user.getUsername());
        if(existingUser != null){
            logger.warn("User "+ user.getUsername() +" already exists");
            throw new SiteUpdateException("User "+ user.getUsername() +" already exists");
        }
        userRepository.create(user);
        final Session session = new Session(user.getUsername());
        sessionRepository.create(session);
        return session.getToken();
    }

    /**
     * This method unregister user. User, his session and all userpages are deleted from repositories.
     * If user is not found method throws SiteUpdateException.
     * If password is wrong method throws SiteUpdateException.
     * @param username
     * @param password
     */
    @Override
    public void unregister(final String username, final String password) {
        final User existingUser = userRepository.get(username);
        if(existingUser == null){
            logger.warn("User " + username + " not found");
            throw new SiteUpdateException("User " + username + " not found");
        }
        if(existingUser.getPassword().equals(password)){
            userRepository.delete(username);
        }else {
            logger.warn("Wrong password");
            throw new SiteUpdateException("Wrong password");
        }

        final Session existingSesssion = sessionRepository.findByUsername(username);
        if(existingSesssion!=null){
            sessionRepository.delete(existingSesssion.getToken());
        }

        final List<UserPage> userPages = userPageRepository.findByUsername(username);
        if(userPages!= null) {
            for(final UserPage userPage : userPages) {
                userPageRepository.delete(userPage.getUserPageId());
            }
        }
    }

    /**
     * Login user. Session is created in sessionRepository.
     * If user not found method throws SiteUpdateException.
     * If session already exists, session token is returned.
     * If user is not found method throws SiteUpdateException.
     * If password is wrong method throws SiteUpdateException.
     * @param username
     * @param password
     * @return token identifier of session
     */
    @Override
    public String login(final String username, final String password) {
        final User existingUser = userRepository.get(username);
        if(existingUser == null){
            logger.warn("User " + username + " not found");
            throw new SiteUpdateException("User " + username + " not found");
        }
        final Session existingSession = sessionRepository.findByUsername(username);
        if(existingSession!=null){
            return existingSession.getToken();
        }
        if(existingUser.getPassword().equals(password)){
            final Session session = new Session(username);
            sessionRepository.create(session);
            return session.getToken();
        }else {
            logger.warn("Wrong password");
            throw new SiteUpdateException("Wrong password");
        }
    }

    /**
     * Logout user. session is deleted in sessionRepository.
     * If session not found method throws SiteUpdateException.
     * @param token identifier of session
     */
    @Override
    public void logout(final String token) {
        final Session existingSession = sessionRepository.get(token);
        if(existingSession == null){
            logger.warn("Wrong token "+ token);
            throw new SiteUpdateException("User session not found");
        }
        sessionRepository.delete(token);
    }
}
