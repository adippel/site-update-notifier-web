package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.clientmodels.PageResponse;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Page;

import java.net.URL;
import java.util.List;

/**
 * Created by adippel on 04.09.2015.
 */
public interface PageService {

    void addPage(String token , Page page);

    List<PageResponse> getPages(String token);

    void deletePage(String token, URL url);

    void checkPages();

}
