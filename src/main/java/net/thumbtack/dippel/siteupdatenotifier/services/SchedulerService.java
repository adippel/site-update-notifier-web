package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by adippel on 02.09.2015.
 * This class consists of methods that executes with scheduled rate.
 */
@Service
public class SchedulerService {
    private static final Logger logger = Logger.getLogger(SchedulerService.class);
    private PageService pageService;
    @Value("${userSession.time}")
    private long sessionTime;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;
    @Autowired
    public SchedulerService(final PageService pageService){
        this.pageService = pageService;
    }

    /**
     * This method executes checking pages for changes fith fixed rate.
     */
    @Scheduled(fixedRateString = "${scheduler.checkPages.fixedRate}")
    public void checkPages(){
        logger.warn("Checking pages " + Thread.currentThread().getName());
        pageService.checkPages();
        logger.warn("Checking pages finishes " + Thread.currentThread().getName());
    }

    @Scheduled(fixedRateString = "${scheduler.checkSession.fixedRate}")
    public void deleteExpiredSession(){
        logger.warn("Checking session " + Thread.currentThread().getName());
        final Date currentDate = new Date();
        final Date checkDate = new Date(currentDate.getTime() - TimeUnit.SECONDS.toMillis(sessionTime));
        sessionRepository.deleteWhereCreationtimeIsEarlier(checkDate);
    }
}
