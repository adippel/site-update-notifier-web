package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.clientmodels.PageResponse;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;
import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserPageDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Page;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.PageContent;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.List;

/**
 * Created by adippel on 02.09.2015.
 * This class consists of methods to operate with UserPage model, userpage's DAO and provide data to link controllers.
 * Class includes standard methods to add, delete, get pages and check for updates.
 * <p>All methods of this class all throw a <tt>NullPointerException</tt>
 */
@Service
public class PageServiceImpl implements PageService {

    private static final Logger logger = Logger.getLogger(PageServiceImpl.class);
    @Autowired
    Converter<List<UserPage>, List<PageResponse>> converter;
    @Autowired
    private PageContentService pageContentService;
    @Autowired
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Autowired
    @Qualifier("primaryUserPageRepository")
    private UserPageDAO userPageRepository;
    @Autowired
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;
    @Autowired
    private MailService mailService;

    /**
     * This method adds page to userPage repository.
     * If such userpage already exists, method throws SiteUpdateException.
     * If page content can't de found, method throws SiteUpdateException.
     * @param token identfier of user session.
     * @param page page data to be added to repository.
     */
    @Override
    public void addPage(final String token, final Page page) {

        final Session existingSession = getSession(token);
        final UserPage existingUserPage = userPageRepository.findByUsernameAndUrl(existingSession.getUsername(), page.getUrl());
        if(existingUserPage != null){
            logger.warn("Page "+ page.getUrl() +" is already in list");
            throw new SiteUpdateException("Page "+ page.getUrl() +" is already in list");
        }
        final UserPage userPage = new UserPage(page, existingSession.getUsername());
        final PageContent pageContent = pageContentService.getPageContent(page.getUrl());
        if(pageContent == null){
            logger.warn("Page with url: " + page.getUrl() + " not found");
            throw new SiteUpdateException("Page with url: " + page.getUrl() + " not found");
        }
        userPage.setDocHash(pageContent.getDocHash());
        userPage.setTextContent(pageContent.getTextContent());
        userPageRepository.create(userPage);
    }

    /**
     * This method returns list of pages that belong to user.
     * After pages returned - their field isChanged set to false to prevent repeat of notify.
     * If user doesn't have pages, method return null.
     * @param token identfier of user session.
     * @return list of page responses that contain fields -url, category, isChanged.
     */
    @Override
    public List<PageResponse> getPages(final String token) {
        logger.warn("Getting " + Thread.currentThread().getName());
        final Session existingSession = getSession(token);
        final List<UserPage> userPages = userPageRepository.findByUsername(existingSession.getUsername());
        if(userPages.size()==0){
            logger.warn("Page not found ");
            return null;
        }
        final List<PageResponse> pageResponses = converter.convert(userPages);
        clearIsChangedField(userPages);
        return pageResponses;
    }

    /**
     * This method deletes userpage from repository.
     * If page not found method throws SiteUpdate exception.
     * @param token identfier of user session.
     * @param url url of page to be deleted.
     */
    @Override
    public void deletePage(final String token, final URL url) {
        final Session existingSession = getSession(token);
        final UserPage existingUserPage = userPageRepository.findByUsernameAndUrl(existingSession.getUsername(), url);
        if(existingUserPage == null){
            logger.warn("User "+ existingSession.getUsername() + " doesn't have page " + url.toString() + " in list to delete it");
            throw new SiteUpdateException("User "+ existingSession.getUsername() + " doesn't have page " + url.toString() + " in list to delete it");
        }
        userPageRepository.delete(existingUserPage.getUserPageId());
    }

    /**
     * This method checks all userpages in repository if there was changes since last check.
     * If changes are detected, method sends email and update userpage content in repository.
     */
    @Override
    public void checkPages(){
        final List<UserPage> userPages = userPageRepository.getAll();
        if(userPages==null) {
            return;
        }
        for(final UserPage userPage : userPages){
            final PageContent newPageContent = pageContentService.getPageContent(userPage.getUrl());
            if(newPageContent != null) {
                final Integer newDocHash = newPageContent.getDocHash();
                if (!userPage.getDocHash().equals(newDocHash)) {
                    final String mail = userRepository.get(userPage.getUsername()).getMail();
                    final String diff = pageContentService.getHTMLDiff(userPage.getTextContent(), newPageContent.getTextContent());
                    mailService.sendMail(mail, userPage.getUsername(), userPage.getUrl(), diff);
                    userPage.setDocHash(newDocHash);
                    userPage.setTextContent(newPageContent.getTextContent());
                    userPage.setChanged(true);
                    userPageRepository.update(userPage);
                }
            }
        }
    }

    /**
     * Set isChanged field to false
     * @param userPages - list of pages to be changed.
     */
    private void clearIsChangedField(final List<UserPage> userPages){
        for (final UserPage userPage : userPages) {
            if (userPage.isChanged()) {
                userPage.setChanged(false);
                userPageRepository.update(userPage);
            }
        }
    }

    /**
     * This method finds and returns session by token.
     * If session not found method throws SiteUpdateException.
     * @param token identfier of user session
     * @return session of user that sends request.
     */
    private Session getSession(final String token){
        final Session existingSession = sessionRepository.get(token);
        if(existingSession == null){
            logger.warn("Wrong token " + token);
            throw new SiteUpdateException("User session not found");
        }
        return existingSession;
    }



}
