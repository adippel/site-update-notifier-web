package net.thumbtack.dippel.siteupdatenotifier.services;

import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;

/**
 * Created by adippel on 04.09.2015.
 */
public interface SessionService {
    String register(User user);

    void unregister(String username, String password);

    String login(String username, String password);

    void logout(String token);
}
