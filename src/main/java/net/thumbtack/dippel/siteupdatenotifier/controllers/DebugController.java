package net.thumbtack.dippel.siteupdatenotifier.controllers;

import net.thumbtack.dippel.siteupdatenotifier.repositories.SessionDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserDAO;
import net.thumbtack.dippel.siteupdatenotifier.repositories.UserPageDAO;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Session;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.UserPage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by adippel on 08.09.2015.
 */

@RestController
@RequestMapping("/api/debug")
public class DebugController {

    @Inject
    @Qualifier("primaryUserRepository")
    private UserDAO userRepository;
    @Inject
    @Qualifier("primaryUserPageRepository")
    private UserPageDAO userPageRepository;
    @Inject
    @Qualifier("primarySessionRepository")
    private SessionDAO sessionRepository;

    public DebugController() {
    }

    public DebugController(final UserDAO userRepository, final UserPageDAO userPageRepository, final SessionDAO sessionRepository) {
        this.userRepository = userRepository;
        this.userPageRepository = userPageRepository;
        this.sessionRepository = sessionRepository;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void clearAll(){
        userPageRepository.deleteAll();
        sessionRepository.deleteAll();
        userRepository.deleteAll();
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<User> getAllUsers(){
        return userRepository.getAll();
    }

    @RequestMapping(value = "/session", method = RequestMethod.GET)
    public List<Session> getAllSession(){
        return sessionRepository.getAll();
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public List<UserPage> getAllPages(){
        return userPageRepository.getAll();
    }

}
