package net.thumbtack.dippel.siteupdatenotifier.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by adippel on 21.09.2015.
 */
@Component
@PropertySource("classpath:cors.properties")
public class CorsFilter implements Filter{

    @Value("${cors.accessControlAllowOrigin}")
    private String accessControlAllowOrigin;
    @Value("${cors.accessControlAllowMethods}")
    private String accessControlAllowMethods;
    @Value("${cors.accessControlMaxAge}")
    private String accessControlMaxAge;
    @Value("${cors.accessControlAllowHeaders}")
    private String accessControlAllowHeaders;
    @Value("${cors.accessControlAllowCredentials}")
    private String accessControlAllowCredentials;

    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException {
        final HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", accessControlAllowOrigin );
        response.setHeader("Access-Control-Allow-Methods", accessControlAllowMethods);
        response.setHeader("Access-Control-Max-Age", accessControlMaxAge);
        response.setHeader("Access-Control-Allow-Headers", accessControlAllowHeaders);
        //response.setHeader("Access-Control-Allow-Credentials", accessControlAllowCredentials);
        chain.doFilter(req, res);
    }

    public void init(final FilterConfig filterConfig) {}

    public void destroy() {}


}

