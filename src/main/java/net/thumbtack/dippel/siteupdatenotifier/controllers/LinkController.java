package net.thumbtack.dippel.siteupdatenotifier.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.*;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.Page;
import net.thumbtack.dippel.siteupdatenotifier.services.PageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;

/**
 * Created by adippel on 02.09.2015.
 */

@RestController
@RequestMapping("/api/link")
public class LinkController {
    private static final Logger logger = Logger.getLogger(LinkController.class);
    @Autowired
    ObjectMapper objectMapper;

    private PageService pageService;

    @Autowired
    private Converter<AddPageRequest, Page> addPageRequestToPageConverter;

    @Autowired
    public LinkController(final PageService pageService) {
        this.pageService = pageService;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void addPage(@RequestHeader(value = "token") final String token, @Valid @RequestBody final AddPageRequest addPageRequest) {
        final Page page = addPageRequestToPageConverter.convert(addPageRequest);
        pageService.addPage(token, page);
    }

    @RequestMapping(method = RequestMethod.DELETE, consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void deletePage(@RequestHeader(value = "token") final String token, @RequestBody final DeletePageRequest deletePageRequest) {
        pageService.deletePage(token, deletePageRequest.getUrl());
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getPages(@RequestHeader(value = "token") final String token){
        final List<PageResponse> pageResponses = pageService.getPages(token);
        if(pageResponses == null){
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        final GetPagesResponse getPagesResponse = new GetPagesResponse(pageResponses);
        try {
            return new ResponseEntity<String>(objectMapper.writeValueAsString(getPagesResponse), HttpStatus.OK);
        } catch (final JsonProcessingException e) {
            logger.warn(e);
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleSiteUpdateException(final Exception e) {
        logger.warn(e);
        return e.getMessage();
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleSiteUpdateException(final SiteUpdateException e) {
        logger.warn(e);
        try {
            return objectMapper.writeValueAsString(new ExceptionResponse(e.getMessage()));
        } catch (final JsonProcessingException e1) {
            logger.warn(e1);
            return null;
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleNotValidException(final MethodArgumentNotValidException e){
        final String message = e.getBindingResult().getFieldError().getField() + " " + e.getBindingResult().getFieldError().getDefaultMessage();
        try {
            return objectMapper.writeValueAsString(new ExceptionResponse(message));
        } catch (final JsonProcessingException e1) {
            logger.warn(e1);
            return null;
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleNotValidException(final ValidationException e){
        logger.warn(e);
        try {
            return objectMapper.writeValueAsString(new ExceptionResponse(e.getMessage()));
        } catch (final JsonProcessingException e1) {
            logger.warn(e1);
            return null;
        }
    }


}
