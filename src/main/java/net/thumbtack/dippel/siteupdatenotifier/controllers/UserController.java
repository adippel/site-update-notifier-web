package net.thumbtack.dippel.siteupdatenotifier.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.ExceptionResponse;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.RegisterUserRequest;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.SessionResponse;
import net.thumbtack.dippel.siteupdatenotifier.clientmodels.UserLoginRequest;
import net.thumbtack.dippel.siteupdatenotifier.exception.SiteUpdateException;
import net.thumbtack.dippel.siteupdatenotifier.servermodels.User;
import net.thumbtack.dippel.siteupdatenotifier.services.SessionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;

/**
 * Created by adippel on 02.09.2015.
 */

@RestController
@RequestMapping("/api/")
public class UserController {
    private static final Logger logger = Logger.getLogger(UserController.class);
    @Autowired
    ObjectMapper objectMapper;
    private SessionService sessionService;
    @Autowired
    private Converter<RegisterUserRequest,User> registerUserRequestToUserConverter;

    @Autowired
    public UserController(final SessionService sessionService) {
        this.sessionService = sessionService;
    }


    @RequestMapping(value  = "user", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String register(@Valid @RequestBody final RegisterUserRequest registerUserRequest){
        final User user = registerUserRequestToUserConverter.convert(registerUserRequest);
        final String token = sessionService.register(user);
        try {
            return objectMapper.writeValueAsString(new SessionResponse(token));
        } catch (final JsonProcessingException e) {
            logger.warn(e);
            return null;
        }
    }

    @RequestMapping(value = "session", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String login(@Valid @RequestBody final UserLoginRequest user){
        final String token = sessionService.login(user.getUsername(), user.getPassword());
        try {
            return objectMapper.writeValueAsString(new SessionResponse(token));
        } catch (final JsonProcessingException e) {
            logger.warn(e);
            return null;
        }
    }

    @RequestMapping(value = "session",method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void logout(@RequestHeader(value = "token") final String token){
        sessionService.logout(token);
    }


    @RequestMapping(value = "user", method = RequestMethod.DELETE, consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void unregister(@Valid @RequestBody final UserLoginRequest user) {
        sessionService.unregister(user.getUsername(), user.getPassword());
    }

    @ExceptionHandler(SiteUpdateException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleSiteUpdateException(final SiteUpdateException e){
        try {
            return objectMapper.writeValueAsString(new ExceptionResponse(e.getMessage())) ;
        } catch (final JsonProcessingException e1) {
            logger.warn(e1);
            return null;
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleNotValidException(final MethodArgumentNotValidException e){
        final ExceptionResponse exceptionResponse = new ExceptionResponse(e.getBindingResult().getFieldError().getField() + " " + e.getBindingResult().getFieldError().getDefaultMessage());
        try {
            return objectMapper.writeValueAsString(exceptionResponse);
        } catch (final JsonProcessingException e1) {
            logger.warn(e1);
            return null;
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleNotValidException(final ValidationException e){
        try {
            return objectMapper.writeValueAsString(new ExceptionResponse(e.getMessage())) ;
        } catch (final JsonProcessingException e1) {
            logger.warn(e1);
            return null;
        }
    }
}
